const CIRCLE_RADIUS = "50px";
const CIRCLES_AMOUNT = 100;

let circlesContainer = null;

window.onload = () => {
    let drawCirclesButton = document.querySelector("input");
    drawCirclesButton.onclick = drawCircles;
}

function drawCircles() {
    const circleRadius = parseInt(window.prompt('Радіус в пікселях:', '10'));

    if (circlesContainer !== null) {
        circlesContainer.remove();
    }
    let containerWidth = `${circleRadius * 2 * 10}px`;
    circlesContainer = buildCirclesContainer(containerWidth);
    document.body.appendChild(circlesContainer);

    for (let i = 0; i < CIRCLES_AMOUNT; ++i) {
        let circle = generateCircle(circleRadius);
        circlesContainer.appendChild(circle);
    }
}

function buildCirclesContainer(width) {
    circlesContainer = document.createElement("div");
    circlesContainer.style.display = "flex";
    circlesContainer.style.flexWrap = "wrap";
    circlesContainer.style.width = width;

    return circlesContainer;
}

function generateCircle(radius) {
    let circle = document.createElement("div");
    circle.style.borderRadius = "50%";
    circle.style.width = radius * 2 + "px";
    circle.style.height = radius * 2 + "px";
    circle.style.backgroundColor = `hsl(${Math.random() * 360}, ${10 + Math.random() * 80}%,${10 + Math.random() * 80}%)`;
    circle.onclick = circle.remove;

    return circle;
}